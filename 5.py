from random import randint

my_dict = dict()

for i in range(10):
    my_dict[chr(97 + i)] = randint(1, 100)

#print(my_dict)

res = sorted(my_dict.keys(), key=lambda x: my_dict[x], reverse=True)
#print(res)

print(*res[:3])