def sum_range(a: int, z: int):
    return (a + z) * (z - a + 1) // 2  # формула сокращенного умножения


print(sum_range(1, 10))
