def is_palindrome(s: str):
    return s == s[::-1]


print(is_palindrome("abba"))
print(is_palindrome("abbac"))