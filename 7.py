def first_not_second(a: list, b: list):
    b_set = set(b)  # помещаем второй список в set для ускорения поиска
    for i in a:
        if i not in b_set:
            print(i)


first_not_second([1, 2, 3, 4], [2, 3, 5, 7])
