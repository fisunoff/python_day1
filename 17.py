class Parrot:
    def __init__(self, breed: str, name: str, age: int):
        self.breed = breed
        self.name = name
        self.age = age

    def repeat_voice(self, voice):
        print(voice)

    def present(self):
        print(f"Я {self.name}, {self.name} - {self.breed}")


barsik = Parrot("Волнистый попугай", "Барсик", 2)
barsik.repeat_voice("Барсик хороший")
barsik.present()
print(barsik.age)