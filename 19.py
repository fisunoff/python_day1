class Bird:
    can_flight = True

    def __init__(self, breed: str, age: int):
        self.breed = breed
        self.age = age

    def __str__(self):
        return f"{self.breed} - {self.age}"

    def get_voice(self):
        print("smbd")


class Parrot(Bird):
    def __init__(self, breed: str, name: str, age: int):
        super().__init__(breed, age)
        self.name = name

    def repeat_voice(self, voice):
        print(voice)

    def present(self):
        print(f"Я {self.name}, {self.name} - {self.breed}")


barsik = Parrot("Волнистый попугай", "Барсик", 2)
barsik.repeat_voice("Барсик хороший")
barsik.present()
print(barsik.age)

raven = Bird("Ворона", 5)
raven.get_voice()
print(raven)

print(isinstance(barsik, Parrot))
print(isinstance(barsik, Bird))
print(isinstance(raven, Parrot))
