call_counter = 0


def call_num(func):
    def wrapper():
        func()
        global call_counter
        call_counter += 1
    return wrapper


@call_num
def hello_world():
    print('Hello world!')


for _ in range(10):
    hello_world()
print(call_counter)